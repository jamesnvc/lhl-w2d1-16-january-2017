//
//  ViewController.m
//  snthstnh
//
//  Created by James Cash on 16-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainThingLabel;
@property (weak, nonatomic) IBOutlet UIButton *byeButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.mainThingLabel.text = @"Hello Lighthouse";

    [self.byeButton addTarget:self
                       action:@selector(sayBye:)
             forControlEvents:UIControlEventTouchUpInside];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sayHi:(UIButton *)sender {
    self.mainThingLabel.text = @"Hi!";
}

- (IBAction)sayBye:(id)sender {
    self.mainThingLabel.text = @"Bye!";
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touched with %lud fingers",(unsigned long) touches.count);
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Moved");
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touch stoped");
}

@end
