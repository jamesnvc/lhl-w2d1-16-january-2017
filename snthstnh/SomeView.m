//
//  SomeView.m
//  snthstnh
//
//  Created by James Cash on 16-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "SomeView.h"

@implementation SomeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touch on our view");
    [self.nextResponder touchesBegan:touches withEvent:event];
}

@end
